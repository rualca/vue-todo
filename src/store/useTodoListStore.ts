import { defineStore } from 'pinia'

type Item = {
  id: number,
  item: string,
  completed: boolean
}

type ItemStore = {
    todoList: Item[],
    id: number
}

export const useTodoListStore = defineStore('todoList', {
  state: (): ItemStore  => ({
    todoList: [],
    id: 0
  }),
  actions: {
    addTodo(item: string) {
      this.todoList.push({ item, id: this.id++, completed: false })
    },
    deleteTodo(itemID: number) {
      this.todoList = this.todoList.filter((object) => object.id !== itemID);
    },
    toggleCompleted(idToFind: number) {
      const todo = this.todoList.find((obj) => obj.id === idToFind);
      
      if (!todo) {
        return
      }

      todo.completed = !todo.completed;
    },
  },
})
